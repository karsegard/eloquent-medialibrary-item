<?php

namespace KDA\Tests\Formats;

use KDA\Eloquent\MedialibraryItem\Flavor\Flavor;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Conversions\Conversion;

class Variants extends Flavor{
    public $collection = "header";
    public $variants= [
        'bw',
    ];
    public $breakpoints = [
        'sm',
        'md',
        'xl',
    ];

    public $stack = [
        'dimensions',
        'ratios',
        'colors'
    ];
   


    public function colors(Media $media,Conversion $conversion, array |null $data) :Conversion {
        return match($this->currentVariant){
            "bw"=> $conversion->greyscale(),
            default=> $conversion
        };
    }


    public function ratios(Media $media,Conversion $conversion,array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'sm'=> $conversion->focalCrop(1024,1024,50,50,2),
            'md'=> $conversion->focalCrop(1024,1024,50,50,2),
            'xl'=> $conversion->focalCrop(1024,1024,50,50,2),
            default=> $conversion
        };
    }

    public function dimensions(Media $media,Conversion $conversion, array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'sm'=> $conversion->width(128)->height(128),
            'md'=> $conversion->width(256)->height(256),
            'xl'=> $conversion->width(512)->height(512),
            default=> $conversion
        };
    }
}