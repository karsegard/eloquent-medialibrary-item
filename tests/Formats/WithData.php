<?php

namespace KDA\Tests\Formats;

use KDA\Eloquent\MedialibraryItem\Flavor\Flavor;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Conversions\Conversion;

class WithData extends Flavor{
    public $collection = "test";
    public $variants= [
    ];
    public $breakpoints = [
        'xl',
    ];

    public $stack = [
        'dimensions',
    ];

    public function dimensions(Media $media,Conversion $conversion,array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'xl'=> $conversion->width($data['width'])->height($data['height']),
            default=> $conversion
        };
    }
}