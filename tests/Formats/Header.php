<?php

namespace KDA\Tests\Formats;

use KDA\Eloquent\MedialibraryItem\Flavor\Flavor;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Conversions\Conversion;

class Header extends Flavor{
    public $collection = "header";
    public $variants= [
        'bw',
        'sepia'
    ];
    public $breakpoints = [
        'sm',
        'md',
        'xl',
    ];

    public $stack = [
        'dimensions',
        'ratios',
        'colors'
    ];


    public function colors(Media $media,Conversion $conversion,array |null $data) :Conversion {
        return match($this->currentVariant){
            "bw"=> $conversion->greyscale(),
            "sepia"=> $conversion->sepia()->blur(20),
            default=> $conversion
        };
    }


    public function ratios(Media $media,Conversion $conversion,array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'xl'=> $conversion->focalCrop(1024,1024,50,50,2),
            default=> $conversion
        };
    }

    public function dimensions(Media $media,Conversion $conversion, array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'md'=> $conversion->width(128)->height(128),
            default=> $conversion
        };
    }
}