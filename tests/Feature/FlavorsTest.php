<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use KDA\Eloquent\MedialibraryItem\Models\Curator;
use Spatie\Image\Image;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Container\BindingResolutionException;
use Spatie\MediaLibrary\Support\UrlGenerator\UrlGeneratorFactory;
class FlavorsTest extends TestCase
{
  use RefreshDatabase;

  public function setUp(): void
  {
    parent::setUp();
    Storage::fake('avatars');
    Storage::fake(config('kda.medialibrary-item.disk'));
    Storage::fake(config('media-library.disk_name'),['root'=>'','url' => '']);
  }

  protected function tearDown(): void
  {

    //empty the public storage folder
    $folder = public_path('storage');
    // $this->removeDirectory($folder);
    parent::tearDown();
  }


  /** @test */
  function add_media_bound_flavor()
  {
    $file = public_path('test.jpg');
    $media = MediaLibraryItem::add($file)->store();

    $p =  \KDA\Tests\Models\Post::factory()->create(['title' => 'test']);
    $p->addMedia($media)->usingFlavor('quick_flavor');
    $keys = array_keys($p->mediaLibraryItems->get(0)->related_flavors->toArray());
    $this->assertEquals($keys, ['quick_flavor']);
  }


  /** @test */
  function add_media_with_wrong_flavor()
  {
    $this->expectException(BindingResolutionException::class);
    // dump(public_path('test.jpg'));
    $media1 = MediaLibraryItem::add(public_path('test.jpg'))->store();
    $p =  \KDA\Tests\Models\Post::factory()->create(['title' => 'test']);
    $p->addMedia($media1)->usingFlavor('\KDA\Tests\Formats\Avatar');
  }

  /** @test */
  function data_are_stored()
  {
    $file = public_path('test.jpg');
    $media = MediaLibraryItem::add($file)->store();
    $p =  \KDA\Tests\Models\Post::factory()->create(['title' => 'test']);
    $p->addMedia($media)->setData(['crop' => '10'])->usingFlavor('\KDA\Tests\Formats\Quick');
    $data = $p->mediaLibraryItems->first()->related_flavors->get('KDA\Tests\Formats\Quick')->first()->data;
    // $keys = array_keys($p->mediaLibraryItems->get(0)->related_flavors->toArray());
    $this->assertEquals($data, ['crop' => '10']);
  }

  /** @test */
  function data_is_usable_in_flavor()
  {
    $file = public_path('test.jpg');
    $media = MediaLibraryItem::add($file)->store();
    $p =  \KDA\Tests\Models\Post::factory()->create(['title' => 'test']);
    $p->addMedia($media)->setData(['width' => '10', 'height' => '10'])->usingFlavor('\KDA\Tests\Formats\WithData');
    $data = $p->mediaLibraryItems->first()->getFirstMediaByFlavor('KDA\Tests\Formats\WithData')->getUrl('xl');
  //  dump($p->mediaLibraryItems->first()->getFirstMediaByFlavor('KDA\Tests\Formats\WithData')->file_name);
    $file =Storage::disk(config('media-library.disk_name'))->get($data);
    $image = Image::load($file);
    

    $this->assertLessThanOrEqual(10, $image->getWidth());
    $this->assertLessThanOrEqual(10, $image->getHeight());
  }


  /** @test */
  function test_flavors_variants()
  {
    $file = public_path('test12.jpg');
    $media = MediaLibraryItem::add($file)->store();
    $p =  \KDA\Tests\Models\Post::factory()->create(['title' => 'test']);
    $p->addMedia($media)->usingFlavor('\KDA\Tests\Formats\Variants');
    $spatiemedia = $p->mediaLibraryItems->first()->getFirstMediaByFlavor('KDA\Tests\Formats\Variants');

    $id = $spatiemedia->id;
    $xl = $spatiemedia->getUrl('xl');
    $md = $spatiemedia->getUrl('md');
    $sm = $spatiemedia->getUrl('sm');

    $bwxl = $spatiemedia->getUrl('bw-xl');
    $bwmd = $spatiemedia->getUrl('bw-md');
    $bwsm = $spatiemedia->getUrl('bw-sm');

    $this->assertEquals("/{$id}/conversions/test12-xl.jpg", $xl);
    $this->assertEquals("/{$id}/conversions/test12-md.jpg", $md);
    $this->assertEquals("/{$id}/conversions/test12-sm.jpg", $sm);
    $this->assertEquals("/{$id}/conversions/test12-bw-xl.jpg", $bwxl);
    $this->assertEquals("/{$id}/conversions/test12-bw-sm.jpg", $bwsm);
    $this->assertEquals("/{$id}/conversions/test12-bw-md.jpg", $bwmd);
  }
}
