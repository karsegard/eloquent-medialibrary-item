<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
use Spatie\Image\Image;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Container\BindingResolutionException;

class RelationshipTest extends TestCase
{
  use RefreshDatabase;
  
  public function setUp(): void
  {
    parent::setUp();
    Storage::fake('avatars');
    Storage::fake(config('kda.medialibrary-item.disk'));
    Storage::fake(config('media-library.disk_name'));
  }
  
  protected function tearDown():void
    {
    
      //empty the public storage folder
      //$folder = public_path('storage');
     // $this->removeDirectory($folder);
      parent::tearDown();
    }

  /** @test */
  function basic_relationships()
  {
    $file = public_path('test.jpg');
    $media = MediaLibraryItem::add($file)->store();
    $p =  \KDA\Tests\Models\Post::factory()->create(['title'=>'test']);
    $p->mediaLibraryItems()->attach($media);
    $this->assertEquals($p->mediaLibraryItems->count(),1);
    $this->assertEquals($media->curators->count(),1);
  }

  /** @test */
 function add_existing_medialibrary_item()
  {
    $file = public_path('test.jpg');
    $media = MediaLibraryItem::add($file)->store();
    $p =  \KDA\Tests\Models\Post::factory()->create(['title'=>'test']);
    $p->addMedia($media)->usingFlavor('\KDA\Tests\Formats\Quick');
    $keys = array_keys($p->mediaLibraryItems->get(0)->related_flavors->toArray());
    $this->assertEquals($keys,['KDA\Tests\Formats\Quick']);
  }

    /** @test */
 function add_existing_medialibrary_from_file()
 {
 
   $p =  \KDA\Tests\Models\Post::factory()->create(['title'=>'test']);
   $p->addMedia(public_path('test.jpg'))->usingFlavor('\KDA\Tests\Formats\Quick');
   $keys = array_keys($p->mediaLibraryItems->get(0)->related_flavors->toArray());
   $this->assertEquals($keys,['KDA\Tests\Formats\Quick']);
 }

    /** @test */
    function ensure_group_is_stored()
    {
    
      $p =  \KDA\Tests\Models\Post::factory()->create(['title'=>'test']);
      $p->addMedia(public_path('test.jpg'))->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');
      $this->assertEquals(1,$p->getMediaItemsByGroup('test')->get()->count());
    }
   /** @test */
   function retrieve_group_items()
   {

    $p =  \KDA\Tests\Models\Post::factory()->create(['title'=>'test']);
    $p->addMedia(public_path('test2.jpg'))->setMultiple(true)->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');
    $p->addMedia(public_path('test3.jpg'))->setMultiple(true)->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');
    $p->addMedia(public_path('test12.jpg'))->setMultiple(true)->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');
    $p->addMedia(public_path('test32.jpg'))->setMultiple(true)->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');
   /* $r =Curator::whereHasMorph('related',get_class($p),function($q)use($p){
      $q->where('curator_id',$p->id);
    })->get();
    dump($r);*/
   }
    
    /** @test */
    function ensure_group_is_not_erased()
    {
    
      $p =  \KDA\Tests\Models\Post::factory()->create(['title'=>'test']);
      $p->addMedia(public_path('test2.jpg'))->usingFlavor('\KDA\Tests\Formats\Quick');

      $p->addMedia(public_path('test.jpg'))->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');
     //$keys = array_keys($p->mediaLibraryItems->get(0)->related_flavors->toArray());

      $this->assertEquals(1,$p->getMediaItemsByGroup()->get()->count());

      $this->assertEquals(1,$p->getMediaItemsByGroup('test')->get()->count());

      $this->assertEquals('test',$p->getMediaItemsByGroup('test')->get()->get(0)->curator->group);
      $this->assertEquals('',$p->getMediaItemsByGroup()->get()->get(0)->curator->group);

      $p->addMedia(public_path('test3.jpg'))->inGroup('test')->usingFlavor('\KDA\Tests\Formats\Quick');

      $this->assertEquals('test',$p->getMediaItemsByGroup('test')->get()->get(0)->curator->group);

      $this->assertEquals(1,$p->getMediaItemsByGroup('test')->get()->count());
    }
   
  
}