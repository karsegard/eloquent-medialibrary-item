<?php

namespace KDA\Tests\Unit;

use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\MedialibraryItem\Models\Curator;
use Spatie\Image\Image;
use KDA\Tests\TestCase;
use Illuminate\Contracts\Container\BindingResolutionException;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use Illuminate\Support\Facades\Storage;
class MediaCreationTest extends TestCase
{
  use RefreshDatabase;
  public function setUp(): void
  {
    parent::setUp();
    Storage::fake('avatars');
    Storage::fake(config('kda.medialibrary-item.disk'));
    Storage::fake(config('media-library.disk_name'));
  }
  
  protected function tearDown():void
    {
    
      //empty the public storage folder
      $folder = public_path('storage');
     // $this->removeDirectory($folder);
      parent::tearDown();
    }

  /** @test */
  function test_create_media_uploaded_default_disk()
  {
    $file = UploadedFile::fake()->image('test.jpg');
    
    $media = MediaLibraryItem::add($file)->store();

    Storage::disk($media->disk)->assertExists($media->original_file_name);
  }


  /** @test */
  function test_create_media_uploaded_another_disk()
  {
  
    $file = UploadedFile::fake()->image('test.jpg');
    
    $media = MediaLibraryItem::add($file)->store('avatars');

    Storage::disk($media->disk)->assertExists($media->original_file_name);
  
  }
  
    /** @test */
  function test_create_media_string()
  {
    
    $file = public_path('test.jpg');
    
    $media = MediaLibraryItem::add($file)->store();
    Storage::disk($media->disk)->assertExists($media->original_file_name);
    
  }

    /** @test */
    function test_create_media_string_another_disk()
    {
      
      $file = public_path('test.jpg');
      
      $media = MediaLibraryItem::add($file)->store('avatars');
      Storage::disk($media->disk)->assertExists($media->original_file_name);
      
    }
  
   /** @test */
   /*function test_create_media_mediaitem()
   {
     $file = UploadedFile::fake()->image('avatar.jpg');
     
     MediaLibraryItem::add($file)->store();
   }
  */
}