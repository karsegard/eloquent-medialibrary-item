<?php

namespace KDA\Tests;

use KDA\Eloquent\MedialibraryItem\ServiceProvider as ServiceProvider;
use Orchestra\Testbench\TestCase as ParentTestCase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Contracts\Console\Kernel;
class TestCase extends ParentTestCase
{

  public function setUp(): void
  {
    parent::setUp();
  }

  protected function getPackageProviders($app)
  {
    return [
      \Spatie\MediaLibrary\MediaLibraryServiceProvider::class,
      ServiceProvider::class
    ];
  }


  protected function runPackageMigrations($app)
  {
    $provider = $this->getPackageProviders($app)[0];
    $traits = class_uses_recursive($provider);

    if (in_array('KDA\Laravel\Traits\HasLoadableMigration', $traits)) {
     
      $this->artisan('migrate', ['--database' => 'mysql'])->run();
      $this->beforeApplicationDestroyed(function () {
        $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
        RefreshDatabaseState::$migrated = false;

      });
    }
  }
  protected function callAfterResolving($name, $callback)
  {
      $this->app->afterResolving($name, $callback);

      if ($this->app->resolved($name)) {
          $callback($this->app->make($name), $this->app);
      }
  }

  protected function loadTestMigrations($paths)
  {
      $this->callAfterResolving('migrator', function ($migrator) use ($paths) {
          foreach ((array) $paths as $path) {
              $migrator->path($path);
          }
      });
  }


  protected function getEnvironmentSetUp($app)
  {
    $this->app = $app;
    $this->app->instance('path.public', __DIR__.DIRECTORY_SEPARATOR.'public');

    $this->app->bind('quick_flavor',\KDA\Tests\Formats\Quick::class);
   /* $app['config']->set('filesystems.disks.testing.driver', 'local');
    $app['config']->set('filesystems.disks.testing.root', realpath(__DIR__.'/public/storage'));
    $app['config']->set('filesystems.disks.uploads.driver', 'local');
    $app['config']->set('filesystems.disks.uploads.root', realpath(__DIR__.'/public/storage'));*/
   /* $app['config']->set('filesystems.disks.avatars.driver', 'local');
    $app['config']->set('filesystems.disks.avatars.root', realpath(__DIR__.'/public/avatars'));*/
    $app['config']->set('filesystems.default', 'testing');
   // $this->app['config']->set('media-library.disk_name','testing');
   // $this->app['config']->set('kda.medialibrary-item.disk','medialibrary');
    
    /*$this->app['config']->set('media-library.media_model',\Spatie\MediaLibrary\MediaCollections\Models\Media::class);
    $this->app['config']->set('media-library.file_namer',\Spatie\MediaLibrary\Support\FileNamer\DefaultFileNamer::class);*/
    $this->loadTestMigrations([__DIR__.'/database/migrations']);
    $this->runPackageMigrations($app);
    
  }
}
