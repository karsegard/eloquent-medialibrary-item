<?php

namespace KDA\Eloquent\MedialibraryItem\Database\Factories;

use KDA\Eloquent\MedialibraryItem\Models\Curator;
use Illuminate\Database\Eloquent\Factories\Factory;

class CuratorFactory extends Factory
{
    protected $model = Curator::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
