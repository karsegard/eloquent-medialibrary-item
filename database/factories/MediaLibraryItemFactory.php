<?php

namespace KDA\Eloquent\MedialibraryItem\Database\Factories;

use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class MediaLibraryItemFactory extends Factory
{
    protected $model = MediaLibraryItem::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
