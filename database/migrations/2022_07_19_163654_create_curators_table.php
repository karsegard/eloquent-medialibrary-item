<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('medialibrary_items', function (Blueprint $table) {
            $table->id();
            $table->string('original_file_name')->nullable();
            $table->string('source_file_name')->nullable();
            $table->string('file_name');
            $table->string('disk');
            $table->text('meta')->nullable();
            $table->timestamps();
        });

        Schema::create('medialibrary_curators', function (Blueprint $table) {
            $table->id();
            $table->foreignId('medialibrary_item_id');
            $table->numericMorphs('curator');
            $table->string('flavor')->nullable();
            $table->string('group')->nullable();
            $table->integer('sort_value')->nullable();
            $table->text('data')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('medialibrary_curators');
        Schema::dropIfExists('medialibrary_items');
        Schema::enableForeignKeyConstraints();
    }
};