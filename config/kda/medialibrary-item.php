<?php


return [
    'resize_source_file'=> true,
    'resize_width'=> 3000,
    'resize_height'=>3000,
    'disk' => 'medialibrary',
    'default_flavor'=>'medialibrary_flavor',
    'max_file_size' => 1024 * 1024 * 15,
];