# eloquent-medialibrary-item

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/eloquent-medialibrary-item.svg?style=flat-square)](https://packagist.org/packages/kda/eloquent-medialibrary-item)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/eloquent-medialibrary-item.svg?style=flat-square)](https://packagist.org/packages/kda/eloquent-medialibrary-item)

## About

This is a package that has the ambition to fill the gap of *spatie/laravel-medialibrary*  and allows to link the same media to multiple model

In addition It provides the concept of Media Flavor, that can centralize and reuse the media conversions

## Installation

You can install the package via composer:

```bash
composer require kda/eloquent-medialibrary-item
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Eloquent\MedialibraryItem" --tag="migrations"
php artisan migrate
```


You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Eloquent\MedialibraryItem" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```


## Usage

### Media Flavor

first, you need to create a media flavor in your project.
Create a class that extends **\KDA\Eloquent\MedialibraryItem\Flavor**.
```php
<?php

namespace App\Flavors;

use KDA\Eloquent\MedialibraryItem\Flavor;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Conversions\Conversion;

class Header extends Flavor{

    //spatie collection name
    public $collection = "header";

    //define your variants
    public $variants= [
        'bw',
        'sepia'
    ];
    // define your breakpoints
    public $breakpoints = [
        'sm',
        'md',
        'xl',
    ];

    //define your stack
    public $stack = [
        'dimensions',
        'ratios',
        'colors'
    ];
   
    //set to false to only generate variants
    //if true, you'll have a conversion with no variant
    public $generateOneWithoutVariant = true;
    
    public function colors(Media $media,Conversion $conversion, string $breakpoint,array |null $data,string | null $variant) :Conversion {
        return match($variant){
            "bw"=> $conversion->greyscale(),
            "sepia"=> $conversion->sepia()->blur(20),
            default=> $conversion
        };
    }


    public function ratios(Media $media,Conversion $conversion, string $breakpoint,array |null $data, string | null $variant) :Conversion {
        return match($breakpoint){
            'xl'=> $conversion->focalCrop(1024,1024,50,50,2),
            default=> $conversion
        };
    }

    public function dimensions(Media $media,Conversion $conversion, string $breakpoint, array |null $data) :Conversion {
        return match($breakpoint){
            'md'=> $conversion->width(128)->height(128),
            default=> $conversion
        };
    }
}
```
### Model
enhance any model with the  **\KDA\Eloquent\MedialibraryItem\Models\Traits\Curates** trait
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Post extends Model 
{
   
    use \KDA\Eloquent\MedialibraryItem\Models\Traits\Curates;
    /// 
```

Then you can associate a media with the addMedia method on your model

```php
    
    $p->addMedia($media1)->usingFlavor('\App\Flavors\Header');

```
This will add the media and generate all the conversions for all breakpoints and variants

Then you can retrieve media like this
    
```php
//reading media 
$p->mediaLibraryItems->first()->getFirstMediaByFlavor('App\Flavors\Header')->getUrl('xl');
//reading variant
$p->mediaLibraryItems->first()->getFirstMediaByFlavor('App\Flavors\Header')->getUrl('bw-xl');
```

### Hint
you can shorten your flavors by binding them in the Service container 

```php
    public function boot()
    {
        $this->app->bind('avatar', \App\Flavors\Avatar::class);
        $this->app->bind('header', \App\Flavors\Header::class);
    }

    ....

    $p->mediaLibraryItems->first()->getFirstMediaByFlavor('header')->getUrl('xl');
```
## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
