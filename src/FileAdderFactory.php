<?php
namespace KDA\Eloquent\MedialibraryItem;

class FileAdderFactory
{
    public function create($model,$file):FileAdder{
        $fileAdder = app(FileAdder::class);

        return $fileAdder
            ->setModel($model)
            ->setFile($file);
    }

    public function add($file):FileAdder{
        $fileAdder = app(FileAdder::class);
        return $fileAdder
            ->setFile($file);
    }
}
