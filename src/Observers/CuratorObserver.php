<?php
 
namespace KDA\Eloquent\MedialibraryItem\Observers;
 
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
 
class CuratorObserver
{
    /**
     * Handle the Curator "created" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function created(Curator $curator)
    {
        //
       $curator->load('media')->media?->createMedias();
    }
 
    /**
     * Handle the Curator "updated" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function updated(Curator $curator)
    {
        //
       // dump($curator->load('curated'));
    }
 
    /**
     * Handle the Curator "deleted" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function deleted(Curator $Curator)
    {
        //
    }
 
    /**
     * Handle the Curator "restored" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function restored(Curator $Curator)
    {
        //
    }
 
    /**
     * Handle the Curator "forceDeleted" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function forceDeleted(Curator $Curator)
    {
        //
    }
}