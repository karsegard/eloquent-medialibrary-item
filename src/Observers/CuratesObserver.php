<?php
 
namespace KDA\Eloquent\MedialibraryItem\Observers;
 
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
 
class CuratesObserver
{
  
 
    /**
     * Handle the Curator "deleted" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function deleting( $model)
    {
        //
        $model->mediaLibraryItems()->detach();
      //  dd('deleted');

    }
 
 
    /**
     * Handle the Curator "forceDeleted" event.
     *
     * @param  \App\Models\Curator  $Curator
     * @return void
     */
    public function forceDeleting( $model)
    {
        //dd('hello');
        //
    }
}