<?php

namespace KDA\Eloquent\MedialibraryItem\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;

class ImportCommand extends Command
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:eloquent-medialibrary-item:import {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import files';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }

    protected function getFiles($path,$allowed_extensions)
    {
     
     
        $result = [];

        if (file_exists($path)) {
            $it = new \RecursiveDirectoryIterator($path);
            foreach (new \RecursiveIteratorIterator($it) as $file) {
                if ($file->isFile()) {
                   if(in_array(strtolower($file->getExtension()),$allowed_extensions))
                    $result[] = $file;
                }
            }
        }

        return $result;
    }
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $allowed_extensions = ['jpg'];
        $path = $this->argument('path');
        $files = $this->getFiles($path,$allowed_extensions);
        $bar = $this->output->createProgressBar(count($files));
        foreach($files as $file ){
            if(MediaLibraryItem::where('file_name',$file->getFileName())->count()==0){
               MediaLibraryItem::add($file->getRealPath())->detectOrientation()->store();
               
            }else{
            }
            $bar->advance();
        }
    }
}
