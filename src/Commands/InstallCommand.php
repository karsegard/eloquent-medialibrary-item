<?php

namespace KDA\Eloquent\MedialibraryItem\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:eloquent-medialibrary-item:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install eloquent-medialibrary-item files';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish',['--provider'=>"\KDA\Eloquent\MedialibraryItem",'--tag'=>"migrations"]);
        $this->call('vendor:publish',['--provider'=>"\KDA\Eloquent\MedialibraryItem",'--tag'=>"migrations"]);
        $this->call('vendor:publish',['--provider'=>"Spatie\MediaLibrary\MediaLibraryServiceProvider",'--tag'=>"migrations"]);
        $this->call('vendor:publish',['--provider'=>"Spatie\MediaLibrary\MediaLibraryServiceProvider",'--tag'=>"config"]);
    }
}
