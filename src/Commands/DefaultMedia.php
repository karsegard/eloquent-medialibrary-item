<?php

namespace KDA\Eloquent\MedialibraryItem\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;

class DefaultMedia extends Command
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:media-library-item:default-media';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate spatie media items ';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }

 
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $result = MediaLibraryItem::all();
        $bar = $this->output->createProgressBar($result->count());
        foreach($result as $c){
            $c->createDefaultMedia();
            $bar->advance();
        }
      
            //if(MediaLibraryItem::where('file_name',$file->getFileName())->count()==0){
               //MediaLibraryItem::add($file->getRealPath())->detectOrientation()->store();
               
            /*}else{
            }
           */
    }
}
