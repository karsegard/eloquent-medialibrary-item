<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints;


class XXLMediaLibrary extends Breakpoint{
    use Traits\Resize;
    public $name= '2xl';
    public $viewport = [1200,1200];
    public $dimensions = [1200,1200];

    
}