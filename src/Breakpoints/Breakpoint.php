<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints;


class Breakpoint
{
    public $name;
    public function __construct($name = null)
    {
        if (!empty($name)) {
            $this->name = $name;
        }
    }
    public function __toString(): string
    {
        return $this->name;
    }
}
