<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints;


class XlMediaLibrary extends Breakpoint{
    use Traits\Resize;
    public $name= 'xl';
    public $viewport = [1024,1024];
    public $dimensions = [1024,1024];

    
}