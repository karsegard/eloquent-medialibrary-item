<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints;


class XXXLMediaLibrary extends Breakpoint{
    use Traits\Resize;
    public $name= '3xl';
    public $viewport = [2400,2400];
    public $dimensions = [2400,2400];

    
}