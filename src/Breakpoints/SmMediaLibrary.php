<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints;


class SmMediaLibrary extends Breakpoint{
    use Traits\Resize;

    public $name= 'sm';
    public $viewport = [256,256];
    public $dimensions = [256,256];
}