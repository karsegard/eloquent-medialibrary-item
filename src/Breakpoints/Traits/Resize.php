<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints\Traits;

use Spatie\MediaLibrary\Conversions\Conversion;

trait Resize{
    
    
    public function resize(Conversion $conversion,array |null $data) : Conversion{
        return $conversion->width($this->dimensions[0])->height($this->dimensions[1]);
    }
    
}