<?php

namespace  KDA\Eloquent\MedialibraryItem\Breakpoints;


class MdMediaLibrary extends Breakpoint{
    use Traits\Resize;

    public $name= 'md';
    public $viewport = [512,512];
    public $dimensions = [512,512];
}