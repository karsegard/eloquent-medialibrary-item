<?php

namespace KDA\Eloquent\MedialibraryItem\Flavor;

use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Conversions\Conversion;

use KDA\Eloquent\MedialibraryItem\Breakpoints\SmMediaLibrary;
use KDA\Eloquent\MedialibraryItem\Breakpoints\MdMediaLibrary;
use KDA\Eloquent\MedialibraryItem\Breakpoints\XlMediaLibrary;
use KDA\Eloquent\MedialibraryItem\Breakpoints\XXLMediaLibrary;
use KDA\Eloquent\MedialibraryItem\Breakpoints\XXXLMediaLibrary;
use Spatie\Image\Manipulations;

class MediaLibrary extends Flavor{
    public $collection = "medialibrary";
    public $variants= [
       'thumb'
    ];
    public $breakpoints = [
        SmMediaLibrary::class,
        MdMediaLibrary::class,
        XlMediaLibrary::class,
        XXLMediaLibrary::class,
    ];

    public $stack = [
        'queued',
        'crop',
        'dimensions',
    ];

    public $generateOneWithoutVariant = false;

    public function queued(Media $media,Conversion $conversion, array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'sm','md'=> $conversion->nonQueued()->sharpen(10),
            default=> $conversion
        };
    }
    public function crop(Media $media,Conversion $conversion, array |null $data) :Conversion {
        return match((string)$this->currentBreakpoint){
            'sm'=> $conversion->crop(Manipulations::CROP_CENTER,$this->currentBreakpoint->dimensions[0],$this->currentBreakpoint->dimensions[1]),
            default=> $conversion
        };
    }
    public function dimensions(Media $media,Conversion $conversion, array |null $data) :Conversion {
        
        return match((string)$this->currentBreakpoint){
            /*'sm'=> $conversion->width(256)->height(256),
            'md'=> $conversion->width(512)->height(512),
            'xl'=> $conversion->width(1024)->height(1024),*/
            'sm','md','xl','2xl','3xl'=> $this->currentBreakpoint->resize($conversion,$data),
            default=> $conversion
        };
    }
}