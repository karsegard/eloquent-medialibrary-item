<?php
namespace KDA\Eloquent\MedialibraryItem\Flavor;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Conversions\Conversion;
use Illuminate\Contracts\Container\BindingResolutionException;
use KDA\Eloquent\MedialibraryItem\Breakpoints\Breakpoint;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;

class Flavor {
    public $collection;
    public $variants= [
        
    ];
    public $breakpoints = [
       
    ];

    public $generateOneWithoutVariant = true;

    protected $currentVariant;
    protected $currentBreakpoint;
    protected $currentConversionName;
    protected $currentMediaLibraryItem;

    public function apply($model,Media $media,array |null $data=null)  {

        
        if($this->generateOneWithoutVariant || count($this->variants)==0){
            $this->generateBreakpoints($model,$media,$data);
        }

        foreach($this->variants as $variant){
            $this->generateBreakpoints($model,$media,$data,$variant);
        }
       
    }

    public function resolveBreakpoint($breakpoint){
        try {
            $bp = resolve($breakpoint);
            return $bp;
        } catch (BindingResolutionException $th) {
            //throw $th;
            return app()->make(Breakpoint::class,['name'=>$breakpoint]);
        }
    }

    public function setConversionName($breakpoint,string | null $variant): Flavor{
        $this->currentConversionName=  !empty($variant) ? $variant."-".(string)$breakpoint: (string)$breakpoint;
        return $this;
    }
    
    protected function setBreakpoint(Breakpoint $breakpoint){
        $this->currentBreakpoint= $breakpoint;
        return $this;
    }
    protected function setMediaLibraryItem(MediaLibraryItem $mediaitem){
        $this->currentMediaLibraryItem= $mediaitem;
        return $this;
    }

    protected function setVariant(string|null $variant){
        $this->currentVariant= $variant;
        return $this;
    }

    public function generateBreakpoints($model,Media $media,array |null $data,$variant=null){
        $stack = $this->stack;
        $this->setMediaLibraryItem($model);
        foreach($this->breakpoints as $breakpoint){
            $breakpoint = $this->resolveBreakpoint($breakpoint);
           // $conversion_name = $this->generateConversionName($breakpoint,$variant);
            $flavor = $this->setConversionName($breakpoint,$variant);
            $conversion = $model->addMediaConversion($this->currentConversionName);
            foreach($stack as $operation){
                $conversion = $flavor
                ->setBreakPoint($breakpoint)
                ->setVariant($variant)
                ->{$operation}($media,$conversion,$data);
            }
            $conversion->performOnCollections($this->collection);
        }
    }
}