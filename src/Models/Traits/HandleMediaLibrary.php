<?php

namespace KDA\Eloquent\MedialibraryItem\Models\Traits;
use Illuminate\Database\Eloquent\Relations\Relation;
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use Illuminate\Support\Facades\Storage;
trait HandleMediaLibrary
{
    public static function bootHandleMediaLibrary(): void
    {
        static::created(function ($model) {
        
            $model->createDefaultMedia();
            
        });
        static::updated(function ($model) {
        //    $model->createMedias();

        });
    }

    public function createDefaultMedia(){
        $this->createFlavor("medialibrary_flavor");
    }
    public function createFlavor($flavor){
        $file = $this->original_file_name; 
        $flavor = resolve($flavor);
        $this->clearMediaCollection($flavor->collection);
        //$disk = config('kda.medialibrary-item.disk');
        $path = Storage::disk($this->disk)->path($file);
        $this->addMedia($path)
            ->usingFileName($this->file_name)
            ->preservingOriginal()
            //->withResponsiveImages()
            ->toMediaCollection($flavor->collection);
    }

    public function getDefaultMediaAttribute(){
        $flavor = resolve("medialibrary_flavor");
        return $this->getFirstMedia($flavor->collection);
    }

    public function createMedias(){
        $this->createDefaultMedia();
        $file = $this->file_name; 
        foreach($this->related_flavors as $flavor_class => $curator){
            if($flavor_class){
         //      $flavor = resolve($flavor_class);
               $this->createFlavor($flavor_class);
            }
        }
    }
}