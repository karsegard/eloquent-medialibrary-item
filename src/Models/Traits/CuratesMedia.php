<?php

namespace KDA\Eloquent\MedialibraryItem\Models\Traits;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use KDA\Eloquent\MedialibraryItem\FileAdderFactory;
use KDA\Eloquent\MedialibraryItem\FileAdder;

use  KDA\Eloquent\MedialibraryItem\Observers\CuratesObserver;
trait CuratesMedia
{

    public static function bootCuratesMedia(): void
    {
        static::observe(CuratesObserver::class);
        static::creating(function ($model)
        {
        });
        static::updating(function ($model)
        {
        });
    }

    public function mediaLibraryItems()
    {
        return $this->morphToMany(MediaLibraryItem::class, 'curator','medialibrary_curators','curator_id','medialibrary_item_id')
                    ->using(Curator::class)-> as ('curator')->withPivot('flavor','group','data','sort_value','id');
    }
/*
    public function scopeMediaForGroup($query, $group)
    {
        $pivot = $this->mediaLibraryItems()->getTable();
    
        $query->whereHas('mediaLibraryItems', function ($q) use ($group, $pivot) {
            $q->where("{$pivot}.group", $group);
        });
    }
*/
    public function getMediaItemsByGroup($group=''){
        return $this->mediaLibraryItems()->wherePivot('group',$group);
    }

    public function getMediaUrls($group='',$flavor='',$conversion=''){
        return $this->getMediaItemsByGroup($group)->get()->reduce(function($medias, $mediaitem) use($flavor,$conversion){
            $result =  $mediaitem->getMediaByFlavor($flavor)->map(function($media) use($conversion){
                return $media->getUrl($conversion);
            });
            return $medias->merge($result);
        },collect([]));
    }

    static public function updateMediaItemsOrders( $query,$updates){
        $medias = $query->get();
      //  dump($medias);
        foreach($medias as $media) {

            if(!$media->curator){
                continue;
            }
         //   dump($media->curator->id);
            $sort_value = $updates[$media->id];
            $media->curator->sort_value=$sort_value;
            $media->curator->save();
        }
    }

    
    public function getMedia($flavor='',$group=''){
        return   $this->mediaLibraryItems()->get()->map(function($m){
            return $m->getMedia();
        });
    }
    // addMedia()->setData()->usingFlavor();
   /* public function addMedia(MediaLibraryItem|string $media,string | array $flavor,array|null $data = null){
        $flavors =[];
        if(!is_array($flavor)){
            $flavors[]=$flavor;            
        }else {
            $flavors=$flavor;
        }
        foreach ($flavors as $flavor){
            $flavor = strpos($flavor,'\\')===0 ? substr($flavor,1): $flavor;
            $f = resolve($flavor);
            $this->mediaLibraryItems()->attach($media,['flavor'=>$flavor,'data'=>$data]);
        }
    }*/

    public function addMedia($file): FileAdder
    {
        return app(FileAdderFactory::class)->create($this, $file);
    }

    public function clearMedia($flavor,$group){
        $this->mediaLibraryItems()->wherePivot('flavor',$flavor)->wherePivot('group',$group)->detach();
    }
    
    
}