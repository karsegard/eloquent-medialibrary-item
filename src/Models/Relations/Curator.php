<?php
namespace KDA\Eloquent\MedialibraryItem\Models\Relations;

use Illuminate\Database\Eloquent\Relations\MorphPivot;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
class Curator extends MorphPivot
{
    protected $table = 'medialibrary_curators';
    protected $guarded = [];
    public $incrementing = true;
    protected $casts = [
        'data'=>'array'
    ];
    /* -------------------------------------------------------------------------- */
    /*                                  RELATIONS                                 */
    /* -------------------------------------------------------------------------- */

    public function curated()
    {
        return $this->belongsTo(MediaLibraryItem::class,'medialibrary_item_id');
    }
    public function media()
    {
        return $this->belongsTo(MediaLibraryItem::class,'medialibrary_item_id');
    }
    public function related()
    {
        return $this->morphTo(__FUNCTION__, 'curator_type', 'curator_id');
    }

}