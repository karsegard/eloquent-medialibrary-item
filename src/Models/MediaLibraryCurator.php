<?php

namespace KDA\Eloquent\MedialibraryItem\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
use KDA\Eloquent\MedialibraryItem\FileAdderFactory;
use KDA\Eloquent\MedialibraryItem\FileAdder;
class MediaLibraryCurator extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    
    protected $table = 'medialibrary_curators';
    protected $fillable = [
        'id',
        'medialibrary_item_id',
        'curator_type',
        'curator_id',
        'flavor',
        'group',
        'sort_value',
        'data',
        'created_at',
        'updated_at'
    ];

    protected $appends = [];

    protected $casts = [
        'id' => 'integer',
        'data' => 'array'
    ];

 
}
