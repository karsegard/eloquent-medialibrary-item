<?php

namespace KDA\Eloquent\MedialibraryItem\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use KDA\Eloquent\MedialibraryItem\Models\Relations\Curator;
use KDA\Eloquent\MedialibraryItem\FileAdderFactory;
use KDA\Eloquent\MedialibraryItem\FileAdder;
class MediaLibraryItem extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Traits\HandleMediaLibrary;
    use \KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
    public $registerMediaConversionsUsingModelInstance = true;
    protected $table = 'medialibrary_items';
    protected $fillable = [
        'id',
        'original_file_name',
        'source_file_name',
        'file_name',
        'disk',
        'meta',
        'created_at',
        'updated_at',
    ];

    protected $appends = [];

    protected $casts = [
        'id' => 'integer',
        'meta' => 'array'
    ];

    protected function applyDefaultAttributes()
    {
        if (empty($this->attributes['disk'])) {
            $this->attributes['disk'] = config('kda.medialibrary-item.disk');
        }
    }
    protected static function newFactory()
    {
        return  \KDA\Eloquent\MedialibraryItem\Database\Factories\MediaLibraryItemFactory::new();
    }

    public function curators()
    {
        return $this->hasMany(Curator::class, 'medialibrary_item_id');
    }

    public function getRelatedModelsAttribute()
    {
        return $this->curators()->get()->groupBy('curator_type');
    }

    public function getRelatedFlavorsAttribute()
    {
        return $this->curators()->get()->groupBy('flavor');
    }

    public function registerDefaultConversions(Media $media = null)
    {
        $default_flavor = config('kda.medialibrary-item.default_flavor','medialibrary_flavor');
        $flavor = resolve($default_flavor);
        $flavor->apply($this, $media);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->registerDefaultConversions($media);
        foreach ($this->related_flavors as $flavor_class => $curators) {
            foreach ($curators as $curator) {
                if ($flavor_class) {
                    $flavor = resolve($flavor_class);
                    $flavor->apply($this, $media, $curator->data);
                }
            }
        }
    }

    public function addMeta($meta){
        $existing = $this->meta ?? [];

        foreach($meta as $k=>  $m){
            $existing[$k]= $m;
        }
        $this->meta = $existing;
        $this->save();
    }

    public function getMediaByFlavor($flavor = null)
    {
        if(!$flavor){
            $flavor =  config('kda.medialibrary-item.default_flavor','medialibrary_flavor');
        }
        $flavor = resolve($flavor);
        return $this->getMedia($flavor->collection);
    }

    

    public function getFirstMediaByFlavor($flavor=null)
    {
        if(!$flavor){
            $flavor =  config('kda.medialibrary-item.default_flavor','medialibrary_flavor');
        }
        return $this->getMediaByFlavor($flavor)?->first();
    }

    public static function add($file){
        return app(FileAdderFactory::class)->add($file);
    }


    public function scopeForGroup($q,$group){
        $pivot = $this->curators()->getRelated()->getTable();
    
        $q->whereHas('curators', function ($q) use ($group, $pivot) {
            $q->where("{$pivot}.group", $group);
        });
    }

    public function getPreviewImageAttribute(){
        return $this->getFirstMediaByFlavor()?->getUrl();
    }
}
