<?php
namespace KDA\Eloquent\MedialibraryItem;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasObservers;
    use \KDA\Laravel\Traits\HasDumps;


    protected $dumps=[
        'medialibrary_curators',
        'medialibrary_items',
    ];
    protected $observers= [
        [Models\Relations\Curator::class=> Observers\CuratorObserver::class]
    ];

    protected $_commands=[
        Commands\ImportCommand::class,
        Commands\InstallCommand::class,
        Commands\RegenerateMedia::class,
        Commands\DefaultMedia::class
    ];
    protected $configs =[
        'kda/medialibrary-item.php'=> 'kda.medialibrary-item'
    ];
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function postRegister(){
        $this->app->bind('medialibrary_flavor',\KDA\Eloquent\MedialibraryItem\Flavor\MediaLibrary::class);

    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
