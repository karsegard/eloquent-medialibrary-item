<?php
namespace KDA\Eloquent\MedialibraryItem;

use Illuminate\Http\File;
use KDA\Eloquent\MedialibraryItem\Flavor\Flavor;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use LiveWire\TemporaryUploadedFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use Spatie\Image\Image;



class FileAdder
{

    protected $model;
    protected $file;
    protected $data;
    protected $file_name;
    protected bool $multiple = false;
    protected $meta = [];
    protected $initialResize=false;
    protected $media;
    protected $group='';

    public function setModel($model):self{
        $this->model = $model;
        return $this;
    }

    public function usingFileName(string $file_name): self
    {
        return $this->setFileName($file_name);
    }

    public function setFileName(string $file_name): self
    {
        $this->file_name = $file_name;

        return $this;
    }

    public function setFile(string | MediaLibraryItem |  UploadedFile $file):self{
        $this->file = $file;
        
    
        if(is_string($file)){
           // $media = MediaLibraryItem::create(['file_name'=>$file]);
          //  $this->setMedia($media);
          $this->setFileName(basename($file));
        }

        if($file instanceof MediaLibraryItem){
            $this->setMedia($file);
        }

        if($file instanceof UploadedFile){
            $this->file = $file;
            $this->setFileName($file->getClientOriginalName());
        }

        return $this;
    }

    public function detectOrientation(){
        if($this->file){
           $image = Image::load($this->file);
           $is_landscape = $image->getWidth()> $image->getHeight();
           //$is_portrait = $image->getWidth()<= $image->getHeight();
           $this->meta['orientation']= $is_landscape ? 'landscape': 'portrait';
        }
        return $this;
    }

    public function resizeSource(){
        $this->initialResize = true;
        return $this;
    }

    public function setData(array $data):self{
        $this->data = $data;
        return $this;
    }

    public function setMedia(MediaLibraryItem $media){
        $this->media = $media;
        return $this;
    }

    public function setMultiple(){
        $this->multiple = true;
        return $this;
    }
    
    public function usingFlavor(string | array $flavor,$disk=''):MediaLibraryItem{
    //    $this->media = $this->store($disk);
        if(!$this->model){
            throw \Exception("no model defined");
        }
        if(!$this->media){
            $this->media = $this->store($disk);
        }
        $flavors =[];
        if(!is_array($flavor)){
            $flavors[]=$flavor;            
        }else {
            $flavors=$flavor;
        }
        foreach ($flavors as $flavor){
            $flavor = strpos($flavor,'\\')===0 ? substr($flavor,1): $flavor;
            if(!$this->multiple){
                $this->model->mediaLibraryItems()->wherePivot('flavor',$flavor)->wherePivot('group',$this->group)->detach();
            }
            $get = $this->model->mediaLibraryItems()->wherePivot('flavor',$flavor)->wherePivot('group',$this->group)->where('medialibrary_item_id',$this->media->id)->get();
            
            if($get->count()==0){
                $this->model->mediaLibraryItems()
                    ->attach($this->media,['flavor'=>$flavor,'data'=>$this->data,'group'=>$this->group]);
            }
        }
        return $this->media;
    }

    public function inGroup($group){
        $this->group = $group;
        return $this;
    }

    public function store($disk=''):MediaLibraryItem{
        $disk = empty($disk) ? config('kda.medialibrary-item.disk') : $disk;
        $storage= Storage::disk($disk);
        //$file->getPath().'/'.$file->getFilename();
        
        if(is_string($this->file)){
            $this->file = $storage->putFile('',new File($this->file));
        }

        if($this->file instanceof UploadedFile){
            $this->file = $this->file->store('',$disk);
        }
        $original_file = $this->file;
        $source_file = $this->file;

        if(config('kda.medialibrary-item.resize_source_file',false) || $this->initialResize ){
            $f= $storage->path($this->file);
            $pathinfo =  pathinfo($f);
            $new_file = $pathinfo['filename']."-source.".$pathinfo['extension'];
            $image = Image::load($f);
            $w = config('kda.medialibrary-item.resize_width');
            $h = config('kda.medialibrary-item.resize_height');
            if($image->getWidth()>$w || $image->getHeight()>$h){
                $image->width($w)->height($h)->save($storage->path($new_file));
                $original_file = $new_file;
            }
        }

        $media = MediaLibraryItem::create(['original_file_name'=>$original_file,'source_file_name'=>$source_file,'disk'=>$disk,'file_name'=>$this->file_name,'meta'=>$this->meta]);



        return $media;
    }
}
